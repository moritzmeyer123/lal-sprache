public class Parser{
    List<Token> tokenliste;
    Token aktuellesToken;

    public Parser(List<Token> pTokenListe){
        tokenliste = pTokenListe;
        tokenliste.toFirst();
        aktuellesToken = tokenliste.getContent();
    }

    public boolean parse(){
        if (tokenliste.hasAccess() && pruefeS()){
            System.out.println("true");
            return true;
        } else{
            System.out.println("Ungültiges Wort");
            return false;
        } 
    }

    //Regel S -> laA, leA, luA
    public boolean pruefeS() {
        if(tokenliste.hasAccess() && aktuellesToken.getTyp() == "EGAL" || aktuellesToken.getTyp() == "ENDE"){
            aktuellesToken = nextToken();
            return pruefeA();
        } else return false;
    }

    //Regel A -> laB, leB, luB
    public boolean pruefeA() {
        if(tokenliste.hasAccess() && aktuellesToken.getTyp() == "EGAL" || aktuellesToken.getTyp() == "ENDE"){
            aktuellesToken = nextToken();
            return pruefeB();
        } else return false;
    }

    //Regel B -> lu
    public boolean pruefeB() {
        if(tokenliste.hasAccess() && aktuellesToken.getTyp() == "ENDE"){
            return true;
        } else return false;
    }

    public Token nextToken(){
        tokenliste.next();
        return tokenliste.getContent();
    }


}