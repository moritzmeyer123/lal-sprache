
public class Scanner {
    List<Token> tokenliste;
    public Scanner(){
        tokenliste = new List<Token>();
    }

    public void scanne(String eingabe){
        char erster, zweiter;
        int pos = 0;
        boolean fehler = false;
        Token aktuellesToken;
        eingabe += '#';
        while(eingabe.charAt(pos) != '#'){
            erster = eingabe.charAt(pos);
            zweiter = eingabe.charAt(pos + 1);
            if (erster == 'l' && zweiter == 'a'){
                aktuellesToken = new Token ("EGAL", "la");
            }
            else if (erster == 'l' && zweiter == 'e'){
                aktuellesToken = new Token ("EGAL", "le");
            }
            else if (erster == 'l' && zweiter == 'u'){
                aktuellesToken = new Token ("ENDE", "lu");
            } else {
                fehler = true;
                break;
            }
            pos = pos + 2;
            tokenliste.append(aktuellesToken);
        }
        System.out.println(fehler);
        ausgabe();
    }

    public void ausgabe(){
        tokenliste.toFirst();
        while(tokenliste.hasAccess()){
            System.out.print(tokenliste.getContent().getTerminal() + " " + tokenliste.getContent().getTyp());
            System.out.println();
            tokenliste.next();
        }

    }

    public List<Token> getTokenList(){
        return tokenliste;
    }

}