public class Token{
    String terminal, typ;
    public Token(String pTyp, String pTerminal){
        terminal = pTerminal;
        typ = pTyp;
    }

    public String getTerminal(){
        return terminal;
    }

    public String getTyp(){
        return typ;
    }

}